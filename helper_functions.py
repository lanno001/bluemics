import shutil
import os
from glob import glob
from datetime import datetime
import pathlib


def parse_output_dir(out_dir, clean=False):
    out_dir = os.path.abspath(out_dir) + '/'
    if clean:
        shutil.rmtree(out_dir, ignore_errors=True)
    pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
    return out_dir


def parse_input_dir(in_dir, pattern=None):
    if type(in_dir) != list: in_dir = [in_dir]
    out_list = []
    for ind in in_dir:
        if not os.path.exists(ind):
            raise ValueError(f'{ind} does not exist')
        if os.path.isdir(ind):
            ind = os.path.abspath(ind)
            if pattern is None: out_list.extend(glob(f'{ind}/**/*', recursive=True))
            else: out_list.extend(glob(f'{ind}/**/{pattern}', recursive=True))
        else:
            if pattern is None: out_list.append(ind)
            elif pattern.strip('*') in ind: out_list.append(ind)
    return out_list

def print_timestamp():
    return datetime.now().strftime('%y-%m-%d_%H:%M:%S')